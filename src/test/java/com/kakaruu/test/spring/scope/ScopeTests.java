package com.kakaruu.test.spring.scope;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class ScopeTests {

    @Test
    @DisplayName("Singleton Scope 테스트")
    public void testSingleton() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ScopeConfig.class);

        SingletonBean bean1 = context.getBean(SingletonBean.class);
        SingletonBean bean2 = context.getBean(SingletonBean.class);

        assertEquals(bean1, bean2);
    }

    @Test
    @DisplayName("Prototype Scope 테스트")
    public void testPrototype() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ScopeConfig.class);

        PrototypeBean bean1 = context.getBean(PrototypeBean.class);
        PrototypeBean bean2 = context.getBean(PrototypeBean.class);

        assertNotEquals(bean1, bean2);
    }
}
