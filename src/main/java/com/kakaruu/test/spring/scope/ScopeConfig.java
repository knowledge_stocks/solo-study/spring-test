package com.kakaruu.test.spring.scope;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan // 현재 패키지를 annotation driven
public class ScopeConfig {

//    Annotation이 아닌 직접 Bean을 등록하고 싶은 경우에는 아래처럼 등록한다.
//    @Bean
//    @Scope("singleton")
//    public SingletonBean singletonBean() {
//        return new SingletonBean();
//    }
//
//    @Bean
//    @Scope("prototype")
//    public PrototypeBean prototypeBean() {
//        return new PrototypeBean();
//    }
}
